package ru.ankorosteleva.webprojecttraining.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.ankorosteleva.webprojecttraining.model.StatusDto;

import java.util.List;

@Slf4j
@RestController
public class PrintController {

    @GetMapping("printPathVariable/{variable}")
    public void printPathVariable(@PathVariable String variable) {
        log.info(variable);
    }

    @PostMapping("printRequestBody")
    public void printRequestBody(@RequestBody StatusDto status) {
        log.info("{}", status);
    }

    @GetMapping("printRequestParam")
    public void printRequestParam(@RequestParam String var1, @RequestParam String var2, @RequestParam List<String> var3) {
        log.info("Параметр 1: " + var1 + ", Параметр 2: " + var2 + ", Параметр 3: " + var3);
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @PostMapping("printResponseBody")
    public StatusDto printResponseBody() {
        return new StatusDto("Param", 672);
    }
}
