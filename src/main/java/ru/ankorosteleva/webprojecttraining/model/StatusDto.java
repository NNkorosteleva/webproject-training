package ru.ankorosteleva.webprojecttraining.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatusDto {

    private String timestamp;
    private Integer status;
}
